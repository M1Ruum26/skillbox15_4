﻿#include <iostream>
#include <string>

void function(const int N, int a)
{
    for (int i = 0; i <= N; ++i)
        if (i % 2 == a)
            std::cout << i << '\t';

    if (a == 0)
        std::cout << " Even Numbers";
    else
        std::cout << " Odd numbers";
}

int main()
{
    int N;
    int a;

    std::cout << "Enter N ";
    std::cin >> N;

    for (int i = 0; i <= N; i = i + 2)
        std::cout << i << '\t';

    std::cout << "Even numbers" << "\n" << "Enter N ";
    std::cin >> N;

    std::cout << "odd or even (1 = odd, 0 = even) ";
    std::cin >> a;

    function(N, a);
}